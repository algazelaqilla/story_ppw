from django.contrib import admin
from .models import activities, person

# Register your models here.

admin.site.register(activities)
admin.site.register(person)
