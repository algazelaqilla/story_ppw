from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms
from .models import activities, person


# class CourseWidget(forms.MultiWidget):

#     def __init__(self, attrs= None):
#         super().__init__([
#             forms.TextInput(),
#             forms.TextInput(),
#         ], attrs)

#     def decompress(self, value):
#         # 'firstvalue secondvalue'
#         if value:
#             return value,split(' ')
#         return ['','']
#         # ['firstvalue',  'secondvalue]

# class CourseField(forms.MultiValueField):

#     def __init__(self,*args, **kwargs):

#         fields = (
#             forms.CharField(), #Test
#             forms.CharField()  #None
#         )

#         super().__init__(*args, **kwargs)

#     def compress(self,data_list):
#         # data_list = ['Test', 'None']
#         return f'{data_list[0]} {data_list[1]}'

# class form_course(forms.Form):
#     course = forms.CharField()
#     lecturer = forms.CharField(label="Lecturer")
#     ccredit = forms.CharField(label='Course Credit')
#     desc = forms.CharField()
#     room = forms.CharField()

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args,**kwargs)

#         self.helper = FormHelper
#         self.helper.form_method = 'post'

#         self.helper.layout = Layout(
#             'course',
#             'ccredit',
#             'desc',
#             'lecturer',
#             'room',
#             Submit('submit', 'Submit', css_class='btn-success')
#         )

class activity(forms.ModelForm):
    class  Meta:
        model = activities
        fields = ('activity',)

class persons(forms.ModelForm):
    class Meta:
        model = person
        fields = ('name',)


