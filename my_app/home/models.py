from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.core.validators import MaxValueValidator, MinValueValidator 


# Create your models here.
# class Snippet(models.Model):
#     course = models.CharField(max_length=100)
#     lecturer = models.CharField(max_length=100)
#     ccredit = models.PositiveIntegerField()
#     desc = models.CharField(max_length= 30,choices=(
#     ('Gasal 2019/2020', 'Gasal 2019/2020'),('Genap 2019/2020', 'Genap 2019/2020'),
#     ('Gasal 2020/2021', 'Gasal 2020/2021'),('Genap 2020/2021', 'Genap 2020/2021'), 
#     ('Gasal 2021/2022', 'Gasal 2021/2022'),('Genap 2021/2022', 'Genap 2021/2022'),
#     ('Gasal 2022/2023', 'Gasal 2022/2023'),('Genap 2022/2023', 'Genap 2022/2023'))
#     )
#     room = models.CharField(max_length=100)

#     def __str__(self):
#         return self.course.title()

# class Entry(models.Model):
#     author = models.ForeignKey(Snippet, on_delete=models.CASCADE)
#     content = models.CharField(max_length=125)
#     published_date = models.DateTimeField(default=timezone.now)

class activities(models.Model):
    activity = models.CharField(max_length=100)

    def __str__(self):
        return self.activity.title()

class person(models.Model):
    name = models.CharField(max_length=100)
    activity = models.ManyToManyField(activities)

    def __str__(self):
        return self.name.title()
