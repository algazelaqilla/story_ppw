from django.test import Client, TestCase
from django.urls import resolve
from .views import kegiatan, add, addperson


# Create your tests here.
class Testkegiatan(TestCase):
    def test_kegiatan_url_exist(self):
        response = Client().get('kegiatan/')
        self.assertEqual(200, response.status_code)
    
    
    def test_add_url_exist(self):
        response = Client().get('add/')
        self.assertEqual(200, response.status_code)


