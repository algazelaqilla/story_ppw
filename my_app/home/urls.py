from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('notup/', views.notup, name='notup'),
    # path('form/', views.courses, name='form'),
    # path('courses/', views.readcourse, name='courses'),
    # path('delete/',views.deletecourse, name='delete'),
    path('kegiatan/',views.kegiatan, name='kegiatan'),
    path('add/',views.add, name='add'),
    path('addperson/<int:pk>/',views.addperson, name='add_person'),

    
]