from django.shortcuts import render, get_object_or_404, redirect
from .forms import activity, persons
from .models import activities, person
from django.contrib import messages



# Create your views here.

def index(request):
    return render(request, 'home/index.html')


def notup(request):
    return render(request, 'home/notup.html')
    
# def course(request):

#     if request.method == 'POST':
#         form = form_course(request.POST)
#         if form.is_valid():

#             course = form.cleaned_data['course']
#             # name = form.cleaned_data['name']
#             # name = form.cleaned_data['name']
#             # name = form.cleaned_data['name']
#             print(course)
            

#     form = form_course()
#     return render(request, 'home/form.html', {'form': form})

def kegiatan(request):
    kegiatan = activities.objects.all()
    orangs = person.objects.all()
    
    return render(request, 'home/kegiatan.html', {'kegiatan':kegiatan, 'person':orangs})

def add(request):
    if request.method == 'POST':
        form = activity(request.POST)
        if form.is_valid():
            form.save()
    form = activity()
    return render(request, 'home/add.html', {'form':form})


def addperson(request, pk):
    if request.method == 'POST':
        form = persons(request.POST['kegiatan'])
        if form.is_valid():
            form.activity = activities.objects.all.get(pk)
            form.save()
            redirect('/')
    form = persons()
    return render(request, 'home/addperson.html', {'form':form})


# def courses(request):

#     if request.method == 'POST':
#         form = SnippetForm(request.POST)
#         if form.is_valid():
            
#             # course = form.cleaned_data['course']
#             # lecturer = form.cleaned_data['lecturer']
#             # ccredit = form.cleaned_data['ccredit']
#             # desc = form.cleaned_data['desc']
#             # room = form.cleaned_data['room']
#             print('VALID')
#             form.save()

#     form = SnippetForm()
#     return render(request, 'home/form.html', {'form': form})

# def readcourse(request):
#     courses = Snippet.objects.all()
#     return render(request, 'home/courses.html', {'courses':courses})

# def deletecourse(request):
#     #POST Request
#     courses = Snippet.objects.all()
#     if request.method == 'POST':
#         course = Snippet.objects.all().get(pk=request.POST['course'])
#         course.delete()
#         return redirect('/')

#     return render(request,'home/delete.html', {'courses':courses})
    
    
